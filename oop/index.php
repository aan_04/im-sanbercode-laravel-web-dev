<?php 
    require_once 'animal.php';
    require_once 'frog.php';
    require_once 'ape.php';
    $sheep = new Animal("shaun");

echo "Name: ".$sheep->name."<br>"; // "shaun"
echo "Legs: ".$sheep->legs."<br>"; // 4
echo "Cold Blooded: ".$sheep->cold_blooded."<br><br><br>"; // "no"

$kodok = new Frog("buduk");

echo "Name: ".$kodok->name."<br>";
echo "Legs: ".$kodok->legs."<br>"; // 4
echo "Cold Blooded: ".$kodok->cold_blooded."<br>"; // "no"
echo "Jump: ";
$kodok->jump();
echo "<br><br><br>"; // "hop hop"

$sungokong = new Ape("kera sakti");

echo "Name: ".$sungokong->name."<br>";
echo "Legs: "; // 4
$sungokong->jumlah_kaki();
echo "<br  >";
echo "Cold Blooded: ".$sungokong->cold_blooded."<br>"; // "no"
echo "Yell: ";
$sungokong->yell() // "Auooo"

?>

